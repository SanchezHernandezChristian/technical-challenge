"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Usuario extends Model {
    toJSON() {
      return {
        ...this.get(),
      };
    }
  }
  Usuario.init(
    {
      id: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      nombre: {
        type: DataTypes.STRING(300),
        allowNull: false,
      },
      segundo_nombre: {
        type: DataTypes.STRING(300),
      },
      apellido_paterno: {
        type: DataTypes.STRING(101),
        allowNull: false,
      },
      apellido_materno: {
        type: DataTypes.STRING(101),
      },
      fecha_de_nacimiento: {
        type: DataTypes.STRING(101),
        allowNull: false,
      },
      email: {
        type: DataTypes.STRING(101),
        allowNull: false,
      },
      telefono: {
        type: DataTypes.BIGINT(11),
        allowNull: false,
      },
    },
    {
      sequelize,
      tableName: "usuarios_christian_sanchez",
      modelName: "Usuario",
      timestamps: false,
    }
  );
  return Usuario;
};
