Instrucciones de uso

Pasos para iniciar la aplicacion:

* Clonar desde el repositorio https://gitlab.com/SanchezHernandezChristian/technical-challenge.git
* Teclear el comando: nodemon

La aplicacion ya tiene la base de datos creada


Rutas de acceso:
La BASE_URL sera determinado por donde se accese a la aplicacion

CRUD Usuario: 
    Obtener Usuarios: get("URL/api/usuarios");
    Crear usuario: post("URL/api/usuarios");
        Datos para crear correctamente: 
            {
                nombre:"",
                segundo_nombre: "",
                apellido_paterno: "",
                apellido_materno: "",
                fecha_de_nacimiento: "".
                email: "",
                telefono: "",
            },
    Editar usuario: put("URL/api/usuarios/:idusuario");
        Datos para crear correctamente (puede ser uno o varios): 
            {
                nombre:"",
                segundo_nombre: "",
                apellido_paterno: "",
                apellido_materno: "",
                fecha_de_nacimiento: "".
                email: "",
                telefono: "",
            },
    Eliminar usuario: delete("URL/api/usuarios/:idusuario");



