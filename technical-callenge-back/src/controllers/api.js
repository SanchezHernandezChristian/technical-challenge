const router = require("express").Router();

const apiUsuario = require("./usuarioController");
router.use("/usuarios", apiUsuario);

module.exports = router;
