"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    queryInterface.createTable("usuarios_christian_sanchez", {
      id: {
        type: Sequelize.INTEGER(11),
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      nombre: {
        type: Sequelize.STRING(300),
        allowNull: false,
      },
      segundo_nombre: {
        type: Sequelize.STRING(300),
        allowNull: false,
      },
      apellido_paterno: {
        type: Sequelize.STRING(101),
        allowNull: false,
      },
      apellido_materno: {
        type: Sequelize.STRING(101),
        allowNull: false,
      },
      fecha_de_nacimiento: {
        type: Sequelize.STRING(101),
        allowNull: false,
      },
      email: {
        type: Sequelize.STRING(101),
        allowNull: false,
      },
      telefono: {
        type: Sequelize.BIGINT(11),
        allowNull: false,
      },
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable("usuarios_christian_sanchez");
  },
};
