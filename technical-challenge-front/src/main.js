import Vue from "vue";
import App from "./App.vue";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";

import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.config.productionTip = false;

import store from "./store";

Vue.mixin({
  data: function() {
    return {
      get API_URL() {
        return "http://localhost:3000";
      },
    };
  },
});

new Vue({
  store,
  render: (h) => h(App),
}).$mount("#app");
