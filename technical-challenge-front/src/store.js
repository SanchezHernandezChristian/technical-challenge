import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    nombre: "",
    segundoNombre: "",
    apellidoPaterno: "",
    apellidoMaterno: "",
    correo: "",
    telefono: "",
    dia: "",
    mes: "",
    year: "",
    menuNombre: 1,
    menuFecha: 0,
    menuContacto: "",
  },
  mutations: {
    mostrarFecha: function(state) {
      state.menuFecha = 1;
    },
    mostrarContacto: function(state) {
      state.menuContacto = 1;
    },
  },
  actions: {
    mostrarFecha(context) {
      context.commit("mostrarFecha");
    },
    mostrarContacto(context) {
      context.commit("mostrarContacto");
    },
  },
});
